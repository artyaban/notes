<h1>Notas</h1>

<h3>Este proyecto es para mostrar un poco de lo que se puede hacer con node.js  y angular2~ </h3>

<h5>La construccion es un API con node.js y express (mongodb) , y en la parte de UI angular2.</h5>
 <br>
Documentacion de Angular-CLI :   <br>
https://github.com/angular/angular-cli/wiki <br>
Documentación de Node.js <br>
https://nodejs.org/es/docs/ <br>
Documentación de Mongo <br>
https://docs.mongodb.com/ <br>
 <br> <br>
"Instrucciones de Instalación." 
  <br>
 BACKEND
  <br>
 1.- Instalar node.js <br>
 2.- Instalar Mongo DB <br>
 3.- Clonar Repositorio <br>
 4.- EN CMD o terminal , run cd ~/notes/API/notas <br>
 5.- run npm install <br>
 6.- actualizar app.js con informacion de base de datos <br>
  <br>
 FRONTEND <br>
  <br>
 1.- En CMD o Terminal  , run cd ~/notes/UI/ui-notas/ <br>
 2.- run npm install <br>
  <br>
 Abrir en explorador http://localhost:4200 <br>
  <br>
 Dudas o comentarios  :  artyaban@gmail.com <br>
 <br>
 Si gustas contribuir para mejorar por favor agregar mensajes para cada push y subir codigo funcional a master. <br>
 <br>
 Gracias... <br>
