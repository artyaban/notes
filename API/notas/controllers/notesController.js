'use strict'

var notes = require('../models/notesModels');
var request = require('request');

exports.clima = function(req, res,lat,lon) {
    

 request.get('http://api.openweathermap.org/data/2.5/weather?lat='+parseFloat(lat)+'&lon='+parseFloat(lon)+'&id=524901&APPID=a630af7fc99b5a86e1d934958f5f7880', function (error, response, body) {
  res.send(response);
});

   
};



exports.index = function(req, res) {
 notes.find({}, function(err, nota) {
           res.send(nota);
        });

};



// Display detail page for a specific notes.
exports.notes_detail = function(req, res , id) {
   
notes.findById(id, function (err, nota) {
  if (err) return handleError(err);
  res.send(nota);
});
};


exports.notes_create = function(req, res) {

	
var data= notes.create({ 
		  titulo: req.body.titulo, 
		 nota:req.body.nota,
		 fecha:req.body.fecha }, function (err,instance) {
  if (err)res.send(err);
 	res.send(instance);
 
});
	
};

exports.notes_update = function(req, res ,id) {


notes.findById(id, function (err, nota) {
  if (err) return handleError(err);

   nota.titulo=req.body.titulo, 
   nota.nota=req.body.nota,
   nota.fecha=req.body.fecha

  nota.save(function (err, updatednotas) {
    if (err) return handleError(err);
    res.send(updatednotas);
  });
});

};


exports.notes_delete = function(req, res,id) {

notes.findByIdAndRemove(id, (err, todo) => {  

    if (err) return res.status(500).send(err);
   
    const response = {
        message: "Nota borrada",
    };
    return res.status(200).send(response);


});

}





