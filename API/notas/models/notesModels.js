var mongoose = require('mongoose'),
    Schema   = mongoose.Schema;

var notesSchema = new Schema({
  titulo:    { type: String },
  nota:     { type: String },
  fecha:  { type: String },

});

module.exports = mongoose.model('Notas', notesSchema);