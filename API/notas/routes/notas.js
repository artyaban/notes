var express = require('express');
var router = express.Router();
var notes_controller = require('../controllers/notesController');


router.get('/',  notes_controller.index)

router.get('/clima/:lat/:lon', 

function(req,res) {
var lat = req.params.lat;
lat= parseFloat(lat);
var lon = req.params.lon;
lon= parseFloat(lon);
notes_controller.clima(req,res,lat,lon);
})



router.get('/:id', function(req,res) {
var id = req.params.id
notes_controller.notes_detail(req,res,id);
})

router.post('/',  notes_controller.notes_create )

router.put('/:id', function(req,res) {
var id = req.params.id
notes_controller.notes_update(req,res,id);
})

router.delete('/:id', function(req,res) {
var id = req.params.id
notes_controller.notes_delete(req,res,id);
})


module.exports = router;
