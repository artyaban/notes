import { Injectable } from '@angular/core';
import { Http, Headers , RequestOptions, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable()
export class NotasService {

  api_url="http://localhost:3000/";
  handleError;
  constructor(private http: Http) { }

  public clima(lat,lon){
   let clima;
  
  return this.http
    .get(this.api_url + 'notas/clima/'+lat+'/'+lon )
    .map(info => clima = info.json() , err=> clima = err)
  .catch(this.handleError);
    }

  public  getAll() {
  let notas;
  return this.http
    .get(this.api_url + 'notas' )
    .map(info => notas = info.json() , err=> notas = err)
	.catch(this.handleError);
    }




     public  postNota(objEnviar) {
  let body  = JSON.stringify(objEnviar);
  let notas;
  return this.http
    .post(this.api_url + 'notas' , objEnviar ).map(info => notas = info.json() 
    , err=> notas = err).catch(this.handleError);
    }


    public deleteNota(id){
     let notas;
     return this.http
    .delete(this.api_url + 'notas/'+id).map(info => notas = info.json() 
    , err=> notas = err).catch(this.handleError);
    }
    
	




    public updateNota(id,objEnviar)
    {
    	let error;

    	return this.http.put(this.api_url +'notas/'+id,objEnviar).map(info => info.json() , err => {
            error =  err;
            console.log(error);
    });
    }



    public getNota(id){


  let error;
      return this.http.get(this.api_url +'notas/'+id).map(info =>{ return info.json() }, err =>{ error =  err;
     
      }).catch(this.handleError);
    }








}
