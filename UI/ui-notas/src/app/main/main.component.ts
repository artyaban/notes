import { Component, OnInit } from '@angular/core';
import { notas } from './notas';
import {FormsModule} from '@angular/forms'
import { NotasService } from '../services/notas.service'; 

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  updateH=true;
  id="";
  notas:notas[]=[{_id:"",titulo:"",nota:"",fecha:""}];
  nota: notas ={_id:"",titulo:"",nota:"",fecha:""};
  location;
  latitud;
  longitud;
  clima;
  constructor(private notasS : NotasService ) { }

  ngOnInit() {

     if(window.navigator.geolocation){
            if(navigator.geolocation){
              navigator.geolocation.getCurrentPosition(position => {
              this.location = position.coords;
              this.latitud=position.coords.latitude;
              this.longitud=position.coords.longitude; 
              this.notasS.clima(this.latitud ,this.longitud ).subscribe(data=>{
              this.clima =   JSON.parse(data.body);
             
              console.log(this.clima);
              });
              });
            }
     }

            
        

  		this.notasS.getAll().subscribe(data=>{
  		this.notas=data;
  		console.log(this.notas);
  		console.log(data);
  	}); 
  }

  guardar(){
  	this.notasS.postNota(this.nota).subscribe(data=>{
  		alert("nota creada");
  		this.notasS.getAll().subscribe(data=>{
  		this.notas=data;
  		}); 

  this.nota.titulo="";
  this.nota.nota="";
  this.nota.fecha="";
  this.id="";

  	});

 
  
  }
  eliminar(id){
  var result = 	confirm("seguro que quieres eliminar la nota ? ")
  	if(result == true)
  	{
  		this.notasS.deleteNota(id).subscribe(data=>{alert(data.message);
  		this.notasS.getAll().subscribe(data=>{
  		this.notas=data;
  		}); 
  		});
  		
  	}
  }

  update(id,titulo,nota,fecha){
  this.updateH=false;
  this.nota.titulo=titulo;
  this.nota.nota=nota;
  this.nota.fecha=fecha;
  this.id=id;
  }
  cancelUpdate(){
  this.updateH=true;
  this.nota.titulo="";
  this.nota.nota="";
  this.nota.fecha="";
  this.id="";
  }

  saveupdate(){
   this.notasS.updateNota(this.id,this.nota).subscribe(data=>{
   	this.notasS.getAll().subscribe(data=>{this.notas=data;});
   	alert("Nota Actualizada");
  this.updateH=true;
  this.nota.titulo="";
  this.nota.nota="";
  this.nota.fecha="";
  this.id="";
   })
  }

}
