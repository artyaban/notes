import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { MainComponent } from './main/main.component';
import { AppRoutingModule } from './app-routing.module';
import {FormsModule} from '@angular/forms'
import { NotasService } from './services/notas.service';
import { HttpClientModule, HttpClientJsonpModule , } from '@angular/common/http';
import { Http ,HttpModule} from '@angular/http';

@NgModule({
  declarations: [
    AppComponent,
    MainComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,HttpModule,

  ],
  providers: [ NotasService],
  bootstrap: [AppComponent]
})
export class AppModule { }
